from firedrake import *

def soln(array):
    if len(array) == 1:
        return "solution"
    else:
        return "solutions"

dpnone = {"partition": True, "overlap_type": (DistributedMeshOverlapType.NONE, 0)}

def plus(x):
    return conditional(gt(x, 0), x, 0)
